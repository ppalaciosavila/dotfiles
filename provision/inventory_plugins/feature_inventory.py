#!/usr/bin/env python3

from os import environ
import platform
from ansible.plugins.inventory import BaseInventoryPlugin
from ansible.errors import AnsibleError

DOCUMENTATION= r'''
    name: feature_inventory
    plugin_type: inventory
    short_description: Returns ansible inventory from host_features file
    description: Returns ansible inventory from host_features file
    options:
      plugin:
        description: Name of the plugin
        required: true
        choices: ['feature_inventory']
'''


class InventoryModule(BaseInventoryPlugin):
    NAME = 'feature_inventory'

    def verify_file(self, path):
        '''Returns true/false if this is possibly a valid file for this plugin
        to consume'''
        valid = False
        if super(InventoryModule, self).verify_file(path):
            if path.endswith(('host-features.yml', 'host-features.yaml')):
                valid = True
        return valid

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path, cache)

        hostname = environ.get('PROVISION_HOSTNAME', platform.node())
        config = loader.load_from_file(path, cache)

        if not config.get('hosts'):
            raise AnsibleError('host_features file must have a "hosts" field defined')

        default_user = config.get('user')
        hosts = config.get('hosts', {})

        host_data = hosts.get(hostname, {})

        host_features = host_data.get('features', [])
        host_user = host_data.get('user', default_user)

        inventory.add_host('localhost')
        inventory.set_variable('localhost', 'user', host_user)

        if not host_user:
            raise AnsibleError('You must define a user in host_features.yml')

        for feature in host_features:
            inventory.add_group(feature)
            inventory.add_host('localhost', group=feature)

        # Detect if OS is Ubuntu like
        with open('/etc/os-release') as f:
            os_data = dict([s.split('=') for s in f.read().split('\n') if s])

        is_debian_like = 'debian' in os_data.get('ID_LIKE', []) or os_data['ID'] == 'debian'
        is_ubuntu_like = 'ubuntu' in os_data.get('ID_LIKE', []) or os_data['ID'] == 'ubuntu'

        inventory.set_variable('localhost', 'distro_debian_like', is_debian_like)
        inventory.set_variable('localhost', 'distro_ubuntu_like', is_ubuntu_like)

        inventory.set_variable('localhost', 'distro_like',
                               'ubuntu' if is_ubuntu_like else
                               'debian' if is_debian_like else os_data['ID'])
